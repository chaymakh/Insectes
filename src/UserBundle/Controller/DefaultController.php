<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\User;
use UserBundle\Form\RegistrationType;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('UserBundle:Default:index.html.twig');
    }

    public function editAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user_id= $this->getUser();
        $user=$em->getRepository("UserBundle:User")->find($user_id);
        $form= $this->createForm(RegistrationType::class,$user);
        $form->add('Modifier', SubmitType::class);

        $form->handleRequest($request);
        if ($form->isValid()){
            $em= $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute("fos_user_profile_show");
        }
        return $this->render('UserBundle:Default:edit_profile.html.twig',array('form'=>$form->createView()));
    }
    function allInsectesAction(){
        $em= $this->getDoctrine()->getManager();
        $insectes= $em->getRepository("UserBundle:User")->findAll();
        return $this->render("UserBundle:Default:allInsectes.html.twig", array("insectes"=>$insectes));
    }

    function mesAmisAction(){
        $em = $this->getDoctrine()->getManager();
        //var_dump($this->getUser()->getId()); die();
        $friends= $em->getRepository("UserBundle:User")->findOneById($this->getUser()->getId());

       //var_dump($friends); die();
       //print_r($friends);
        return $this->render("UserBundle:Default:mesAmis.html.twig", array('friends'=>$friends));
    }
    function tousLesAmisAction(){
        $em = $this->getDoctrine()->getManager();
        //var_dump($this->getUser()->getId()); die();
        $friends= $em->getRepository("UserBundle:User")->findAll();

        //var_dump($friends); die();
        //print_r($friends);
        return $this->render("UserBundle:Default:tousLesAmis.html.twig", array('friends'=>$friends));
    }

    function ajouterUnAmiAction($id){
        $em = $this->getDoctrine()->getManager();
        //var_dump($this->getUser()->getId()); die();
        $friends= $em->getRepository("UserBundle:User")->findOneById($this->getUser()->getId());
        $newFriend= $em->getRepository("UserBundle:User")->findOneById($id);
       // var_dump($newFriend); die();
        $friends->addMyFriend($newFriend);
            $em->flush();
       return $this->redirectToRoute("mes_amis");

    }
    function detailsAmiAction($id){
        $em= $this->getDoctrine()->getManager();
        $ami= $em->getRepository("UserBundle:User")->findById($id);
        //var_dump($insecte); die();
        return $this->render("UserBundle:Default:details.html.twig", array("insecte"=>$ami));
    }
    function supprimerAmiAction($id){
        $em= $this->getDoctrine()->getManager();
        $ami= $em->getRepository("UserBundle:User")->findOneById($id);
        $em->remove($ami);
        $em->flush();
        return $this->redirectToRoute("mes_amis");
    }
}
